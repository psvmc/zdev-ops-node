const path = require("path");
const {
  VueLoaderPlugin
} = require('vue-loader')
var HtmlWebpackPlugin = require('html-webpack-plugin'); //打包html的插件

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  entry: {
    index: "./src/index.js",
  },

  output: {
    path: __dirname + "/dist",
    publicPath: "./",
    filename: "[name].js",
  },

  plugins: [
    // 请确保引入这个插件！
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html' //只增加了这行
    })
  ],
  module: {
    rules: [{
        test: /\.(png|jpg|gif|svg)$/,
        loader: "file-loader",
        options: {
          esModule: false,
          query: {
            name: "[name].[ext]?[hash]",
          },
        },
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      // 它会应用到普通的 `.js` 文件
      // 以及 `.vue` 文件中的 `<script>` 块
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      // 它会应用到普通的 `.css` 文件
      // 以及 `.vue` 文件中的 `<style>` 块
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      }
    ]
  },

  resolve: {
    extensions: [".js", ".vue", ".json"],
    alias: {
      "@": resolve("src"),
      images: resolve(path.join(__dirname, "src", "assets", "images")),
    },
  },
};