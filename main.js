const Koa = require('koa')
const app = new Koa()
const Router = require('koa-router');
const path = require('path');
const serve = require('koa-static');
const router = new Router()

const static_serve = serve(path.join(__dirname, "dist"));
app.use(static_serve);

app.use(router.routes()).use(router.allowedMethods());
router.get('/hello', (ctx, next) => {
  ctx.body = 'Hello World';
})

app.listen(3000)